#include "project.h"

void setup() {
    Serial.begin(9600);
    Serial.println("on8tom morse");
}

void loop() {
    morseString("ON8TOM");
    serialDelay( 10 * M_TIME);
}

void strobe(uint32_t on, uint32_t off) {
    digitalWrite(LED, 1);
    serialDelay(on);

    digitalWrite(LED, 0);
    serialDelay(off);
}

void morseString(const char* s) {

    while (*s != 0) {

        char c = tolower(*s);

        for (uint8_t i = 0; i < sizeof (morseAlphabet) / 7; i++) {
            char C = morseAlphabet[i][0];

            if (C == ' ') {
                serialDelay(2 * M_TIME);
                continue;
            }

            if (C == '\n') {
                serialDelay(3 * M_TIME);
            }

            if (C == c) {

                for (uint8_t j = 1; j < 7; j++) {
                    char t = morseAlphabet[i][j];
                    if (t == 0)
                        break;
                    strobe(M_TIME * t, M_TIME);
                }
                serialDelay(M_TIME);
                break;
            }
        }


        s++;
    }

}

void serialDelay(uint32_t time){
    uint32_t next = millis() + time;
    
    while (millis() < next){
        if(Serial.available()){
            Serial.write(Serial.read());
        }
    }
}